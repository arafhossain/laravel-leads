<?php
/**
 * Created by PhpStorm.
 * User: noob
 * Date: 3/22/18
 * Time: 2:29 AM
 */

class Dada
{
    public $brandName = "xiomi";
    public $model;

    public function __construct($modelName = '')
    {
        $this->model = $modelName;
    }

    public function getMobileDetails()
    {
        $this->model = 'Anything....';
    }

    public function getBrandName()
    {
        return $this->brandName;
    }

}

class Baba extends Dada
{
    public static function getNewBrandName()
    {
        echo "Hey how are you? ";
    }
}

// can call this static function without creating the object.
Baba::getNewBrandName();

/*$obj_newBrandName = new Baba();
$obj_newBrandName::getNewBrandName();*/