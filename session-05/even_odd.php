
<!DOCTYPE html>
<html>
<head>
    <title>Even or Odd</title>
    <style type="text/css">
        form {
            width: 400px;
            margin: 0 auto;
            margin-top: 10%;
            border: 7px double #000;
            padding: 10px;
        }
        table {
            width: 100%;
        }
        table>tbody>tr>td {
            width: 100%;
        }
        input[type='text'] {
            padding: 5px 9px;
        }
        input[type='submit'] {
            width: 50%;
            margin-top: 7%;
            margin-left: 25%;
            padding: 5px 6px;
        }
    </style>
</head>
<body>
<form method="post" action="functions/even_odd.php">
    <h1 align="center">Even or Odd Checking</h1>
    <table>
        <tbody>
        <tr>
            <td>Enter a number</td>
            <td><input type="text" name="number"></td>
        </tr>
    </table>
    <input type="submit" name="num" value="Even or Odd">
</form>
</body>
</html>