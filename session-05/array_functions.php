<?php 
echo "<pre>";

// array()
echo "***** <b>array()</b> ****** </br></br>";

$students = array('Kuddus','Pukkas','Mokles','Komba');
$studentsLength = count($students);
for ($s=0; $s < $studentsLength; $s++) { 
	echo $students[$s];
	echo "<br>";
}
	echo "<br><br>";


// array_chunk()
echo "***** <b>array_chunk()</b> ****** </br></br>";

$month = array('Jan','Feb','March','April');
print_r(array_chunk($month,2));

	echo "<br><br>";

// array_combine()
echo "***** <b>array_combine()</b> ****** </br></br>";

$comb = array_combine($students,$month);
print_r($comb);

	echo "<br><br>";

// array_count_values()
echo "***** <b>array_count_values()</b> ****** </br></br>";

$count = ['Jan','February','Jan','March','March'];
print_r(array_count_values($count));

	echo "<br><br>";

// array_diff()
echo "***** <b>array_diff()</b> ****** </br></br>";

print_r(array_diff($month,$count));

// array_diff_key()
echo "***** <b>array_diff_key()</b> ****** </br></br>";

$fruits1 = array("a"=>"Apple","b"=>"Ball","c"=>"Cat");
$fruits2 = array("a"=>"Apple","c"=>"Cat","d"=>"Doll");

print_r(array_diff_key($fruits1 , $fruits2));

	echo "<br><br>";

// array_key_exists()
echo "***** <b>array_key_exists()</b> ****** </br></br>";

$phn = array("SONY"=>"Xperia Z","IPhone"=>"5");

if (array_key_exists("SONY",$phn)) {
  echo "Key exists!";
}
else {
  echo "Key does not exist!";
}

	echo "<br><br>";

// array_keys()
echo "***** <b>array_keys()</b> ****** </br></br>";

print_r(array_keys($comb));

	echo "<br><br>";

// array_merge()
echo "***** <b>array_merge()</b> ****** </br></br>";

print_r(array_merge($month , $count));

	echo "<br><br>";

// array_pad()
echo "***** <b>array_pad()</b> ****** </br></br>";

print_r(array_pad($month,6,"May"));

	echo "<br><br>";

// array_pop()
echo "***** <b>array_pop()</b> ****** </br></br>";

array_pop($count);
print_r($count);

	echo "<br><br>";

// array_push()
echo "***** <b>array_push()</b> ****** </br></br>";

array_push($count,'$400','March');
print_r($count);

	echo "<br><br>";

// array_rand()
echo "***** <b>array_rand()</b> ****** </br></br>";

$random_keys = array_rand($count,3);
var_dump($random_keys);

	echo "<br><br>";

// array_replace()
echo "***** <b>array_replace()</b> ****** </br></br>";

$letter = ['one','two','three','four'];
$romanLetter = ['I','II','III','IV'];

print_r(array_replace($letter,$romanLetter));

	echo "<br><br>";

// array_search()
echo "***** <b>array_search()</b> ****** </br></br>";

print_r(array_search('$400',$count));

	echo "<br><br>";

// array_shift()
echo "***** <b>array_shift()</b> ****** </br></br>";

echo array_shift($letter).'<br>';
print_r($letter);

	echo "<br><br>";

// array_unshift()
echo "***** <b>array_shift()</b> ****** </br></br>";

echo array_unshift($letter, 'one');
print_r($letter);

	echo "<br><br>";

// array_unique()
echo "***** <b>array_unique()</b> ****** </br></br>";

print_r(array_unique($count));

	echo "<br><br>";

// array_sum()
echo "***** <b>array_sum()</b> ****** </br></br>";

$num = [10,20,30,10,50];

echo array_sum($num);

	echo "<br><br>";

// arsort()
echo "***** <b>arsort()</b> ****** </br></br>";

// $num2 = ['one' => 10, 'two' => 20, 'three' => 30, 'four' => 10, 'five' => 50];
$num = [10,20,30,10,50];
arsort($num);
print_r($num);

echo "***** <b>asort()</b> ****** </br></br>";

asort($num);
print_r($num);

	echo "<br><br>";

// count()
echo "***** <b>count()</b> ****** </br></br>";

echo count($num);

	echo "<br><br>";

// end()
echo "***** <b>end()</b> ****** </br></br>";

echo end($num);

	echo "<br><br>";

// in_array()
echo "***** <b>in_array()</b> ****** </br></br>";

if (in_array(10,$num)) {
	echo "found the number 10";
}

	echo "<br><br>";

// shuffle()
echo "***** <b>shuffle()</b> ****** </br></br>";

shuffle($letter);
print_r($letter);

	echo "<br><br>";
