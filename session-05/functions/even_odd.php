<?php

if (isset($_POST['num'])) {

    $num = $_POST['number'];

    if (!isset($num)) {
        echo "Please, fill all the fields correctly";
        die();
    }
    elseif (!is_numeric($num)) {
        echo "Please enter integer numbers only";
        die();
    }
    else {
        if ($num % 2 == 0) {
            echo '<h1>'.$num.' is even.</h1>';
        }
        else {
            echo '<h1>'.$num.' is odd.</h1>';
        }
    }
}
