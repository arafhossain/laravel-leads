<?php
/**
 * Created by PhpStorm.
 * User: noob
 * Date: 3/22/18
 * Time: 11:50 PM
 */

namespace App\Users;
use PDO;
class Users
{
    public $username = '';
    public $password = '';

    public function setData($data)
    {
        $this->username = $data['username'];
        $this->password = $data['password'];
    }

    public function store()
    {
        try {
            date_default_timezone_set('Asia/Dhaka');

            $dateTime = date('Y-m-d H:i:s');

            $updatedTime = '1888-12-30 12:34:21';

            $conn = new PDO('mysql:host=localhost;dbname=cms','root','');
            $query = "INSERT INTO users(username,password,created_at,updated_at) VALUES (:usr,:pass,:create_at,:updated_at)";
            $stmt = $conn->prepare($query);
            $stmt->execute(array(
                ':usr' => 'Araf Hossain',
                ':pass' => '12345',
                ':create_at' => $dateTime,
                ':updated_at' => $updatedTime

            ));

        } catch (\PDOException $e) {
            echo "Error: ". $e->getMessage();
        }
    }

}