
<!DOCTYPE html>
<html>
<head>
	<title>Grading System</title>
	<style type="text/css">
		form {
			width: 300px;
			margin: 0 auto;
			margin-top: 10%;
			border: 7px double #000;
			padding: 10px;
		}
		table {
			width: 100%;
		}
		table>tbody>tr>td {
			width: 100%;
		}
		input[type='text'] {
			padding: 5px 9px;
		}
		input[type='submit'] {
			width: 50%;
			margin-top: 7%;
			margin-left: 28%;
			padding: 5px 6px;
		}
	</style>
</head>
<body>
	<form method="post" action="function/grad_func.php">
		<h1 align="center">Grading System</h1>
		<table>
			<tbody>
				<tr>
					<td>Subject 1</td>
					<td><input type="text" name="sub[]"></td>
				</tr>
				<tr>
					<td>Subject 2</td>
					<td><input type="text" name="sub[]"></td>
				</tr>
				<tr>
					<td>Subject 3</td>
					<td><input type="text" name="sub[]"></td>
				</tr>
				<tr>
					<td>Subject 4</td>
					<td><input type="text" name="sub[]"></td>
				</tr>
			</tbody>
		</table>
		<input type="submit" name="result" value="Result">
	</form>
</body>
</html>