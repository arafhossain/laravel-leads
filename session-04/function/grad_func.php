<?php 

if (isset($_POST['result'])) {
	// variable declaration part
	$subs = $_POST['sub'];
	$points = [];
	$msg = $sum = $grade = NULL;

	// checking the input numbers
	foreach ($subs as $sub ) {
		if (!isset($sub)) {
			echo "<h3 style='text-transform: capitalize;color:red;text-align:center;margin-top:10%;'>Please, fill all the fields correctly</h3>";

			die;
		}
		elseif (!is_numeric($sub)) {
			echo "<h3 style='text-transform: capitalize;color:red;text-align:center;margin-top:10%;'>Please, enter integer numbers only</h3>";

			die;
		}
		elseif ($sub > 100 || $sub < 0) {
			// $msg = "";
			echo "<h3 style='text-transform: capitalize;color:red;text-align:center;margin-top:10%;'>Please, please input valid numbers</h3>";

			die;
		}
	}

	// doing calculation in here
	foreach ($subs as $sub => $value) {
		// checking '0' in an array
		if (in_array( 0 , $subs)) {
			$msg = "You failed in the exam";
			break;
		}
		else {
			for ($i = 0; $i < count($subs); $i++) {
				
				if ($value >= 80 && $value <=100) {
					// $subj = isset($subs[$sub]) ? $subs[$sub] : ''; 
					$points[$sub] = 5;
					break;
				}
				elseif ($value >= 70 && $value <=79) {
					$points[$sub] = 4;
					break;
				}
				elseif ($value >= 60 && $value <=69) {
					$points[$sub] = 3.5;
					break;
					// array_push($points,3.5);
				}
				elseif ($value >= 50 && $value <=59) {
					$points[$sub] = 3;
					break;
					// array_push($points,3);
				}
				elseif ($value >= 40 && $value <=49) {
					$points[$sub] = 2;
					break;
					// array_push($points,2);
				}
				elseif ($value >= 33 && $value <=39) {
                    $points[$sub] = 1;
					break;
					// array_push($points,1);
				}
				elseif ($value >= 0 && $value <=32) {
					$points[$sub] = 0;
					break;
					// array_push($points,0);
				}
			}
		}
		// doing summation all the points in array
		$sum = array_sum($points);	
	}

	// dividing the all points. 
	$grade = $sum / 4;

	// printing message;
	if (!empty($grade)) {
		echo "<h2 style='text-transform: capitalize;color:green;text-align:center;margin-top:10%;'>You passed the exam<br>And your grade is<b> $grade </b></h2>";
	}
	else {
		echo "<h1 style='text-transform: capitalize;color:red;text-align:center;margin-top:10%;'>".$msg."</h1>";
	}
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Grading result</title>
</head>
<body style="background-color: black;">

</body>
</html>