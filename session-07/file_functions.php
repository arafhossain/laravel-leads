<?php 

// basename()
echo "***** <b>basename()</b> ****** </br></br>";

$path = "/session-7/file_functions.php";
echo basename($path,".php");

	echo "<br><br>";

// chmod()
echo "***** <b>chmod()</b> ****** </br></br>";

echo "<pre>
	This is for permission of a file.
	-> The first number is always zero.
	-> Second number specifies permissions for the owner.
	-> Third number specifies permissions for the owner's user group.
	-> Fourth number specifies permissions for everybody else. 

	To set Multiple permissions, add up the following numbers. 
	* 1 = execute permissions
	* 2 = write permissions
	* 4 = read permissions
	</pre>";
	chmod("test.html", 0600);

	echo "<br><br>";

// copy()
echo "***** <b>copy()</b> ****** </br></br>";

echo copy("test.html" , "oop.html");

	echo "<br><br>";
	echo ""

// copy()
echo "***** <b>copy()</b> ****** </br></br>";


 ?>