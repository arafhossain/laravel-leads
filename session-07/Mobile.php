<?php
/**
 * Created by PhpStorm.
 * User: noob
 * Date: 3/22/18
 * Time: 12:51 AM
 */

class Mobile
{
    public $brandName;
    public $model;

    public function __construct($getBrandName)
    {
        $this->brandName = $getBrandName;
    }

    public function getMobileDetails()
    {
        echo $this->brandName;
    }

}

class newBrand extends Mobile
{

}

$newBrandObj = new newBrand('Nokia 6');

$newBrandObj->getMobileDetails();

/*$constructObj1 = new Mobile('iPhone 5');

$constructObj2 = new Mobile('New phone');
$constructObj2->getMobileDetails();*/
