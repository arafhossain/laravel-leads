<?php
/**
 * User: noob
 * Date: 2/19/18
 * Time: 11:40 PM
 */

echo '<h2>Example for __LINE__ </h2>';
echo 'The line number is'.__LINE__.'<br><br>';

echo '<h2>Example for __FILE__ </h2>';
echo 'The file name is'.__FILE__.'<br><br>';

echo '<h2>Example for __DIR__ </h2>';
echo __DIR__.'<br><br>';
echo dirname(__FILE__);


echo '<h2>Example for __FUNCTION__ </h2>';
function cash() {
    echo "The function is".__FUNCTION__."<br><br>";
}
cash();

// using magic constants outside function give the blank output
function test_function() {
    echo "hello world";
}
test_function();

echo __FUNCTION__."<br><br>";

echo '<h2>Example for __CLASS__ </h2>';
class alu {
    public function __construct()
    {
        ;
    }

    function alu_method() {
        echo __CLASS__."<br><br>";
    }
}

$a = new alu;
$a -> alu_method();
class first {
    function test_first() {
        echo __CLASS__;
    }
}

class second extends first {
    public function __construct()
    {
        ;
    }
}

$a = new second;
$a -> test_first();


echo '<h2>Example for __TRAIT__ </h2>';
trait created_trait {
    function alu() {
        echo __TRAIT__;
    }
}

class anew {
    use created_trait;
}

$b = new anew;
$b -> alu();

echo '<h2>Example for __METHOD__ </h2>';
class meth {
    public function __construct()
    {
        echo __METHOD__."<br><br>";
    }

    public function meth_fun() {
        echo __METHOD__."<br><br>";
    }
}

$b = new meth;
$b -> meth_fun();

echo '<h2>Example for __NAMESPACE__ </h2>';
class name {
    public function __construct()
    {
        echo "This line will be printed on calling namespace";
    }
}

$class_name= __NAMESPACE__."\name";
$b = new $class_name;