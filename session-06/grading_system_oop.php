<?php 

require_once("grad_func_oop.php");

if (isset($_POST['submit'])) {
	
	$obj_grad = new grading();

	if (!empty($grade)) {
		echo "<h2 style='text-transform: capitalize;color:green;text-align:center;margin-top:10%;'>You passed the exam<br>And your grade is<b>". $obj_grad->gradingSystem()."</b></h2>";
	}
	else {
		echo "<h1 style='text-transform: capitalize;color:red;text-align:center;margin-top:10%;'>".$msg."</h1>";
	}
}


?>

<!DOCTYPE html>
<html>
<head>
	<title>Grading System</title>
	<style type="text/css">
		form {
			width: 300px;
			margin: 0 auto;
			margin-top: 10%;
			border: 7px double #fff;
			padding: 10px;
			color: #fff;
		}
		table {
			width: 100%;
		}
		table>tbody>tr>td {
			width: 100%;
		}
		input[type='text'] {
			padding: 5px 9px;
		}
		input[type='submit'] {
			width: 50%;
			margin-top: 7%;
			margin-left: 28%;
			padding: 5px 6px;
		}
	</style>
</head>
<body>
	<form method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
		<h1 align="center">Grading System</h1>
		<table>
			<tbody>
				<tr>
					<td>Subject 1</td>
					<td><input type="text" name="sub[]"></td>
				</tr>
				<tr>
					<td>Subject 2</td>
					<td><input type="text" name="sub[]"></td>
				</tr>
				<tr>
					<td>Subject 3</td>
					<td><input type="text" name="sub[]"></td>
				</tr>
				<tr>
					<td>Subject 4</td>
					<td><input type="text" name="sub[]"></td>
				</tr>
			</tbody>
		</table>
		<input type="submit" name="result" value="Result">
	</form>
</body>
</html>