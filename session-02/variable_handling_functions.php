<?php



// empty()
echo "***** <b>empty()</b> ****** </br></br>";
 $var=0;
 if (empty($var)) {
     echo 'This variable is empty<br><br>';
 }

 $expected_array_got_string='something';
 var_dump(empty($expected_array_got_string['some_key'])); echo '<br>';
 var_dump(empty($expected_array_got_string[0])); echo '<br>';
 var_dump(empty($expected_array_got_string['0'])); echo '<br>';
 var_dump(empty($expected_array_got_string[0.5])); echo '<br>';
 var_dump(empty($expected_array_got_string['0.5'])); echo '<br>';
 var_dump(empty($expected_array_got_string['0 mostel'])); echo '<br><br>';

 // get_resource_type()
echo "***** <b>get_resource_type()</b> ****** </br></br>";
 $fp=fopen("test","w");
 echo get_resource_type($fp)."<br>";


 // gettype()
echo "***** <b>gettype()</b> ****** </br></br>";
$data=array(1,1.,NULL, new stdClass,'foo');

foreach ($data as $value) {
    echo gettype($value),"\n";
}
echo "<br><br>";

// is_array()
echo "***** <b>is_array()</b> ****** </br></br>";
$yes=array('this','is','an array');

echo is_array($yes) ? 'Array':'not an Array';
echo "<br>";

$no='this is a string';
echo is_array($no) ? 'Array' : 'not an Array';
echo "<br><br>";

// is_bool()
echo "***** <b>is_bool()</b> ****** </br></br>";
$a = false;
$b = 0;

if (is_bool($a) === true) {
    echo "Yes, this is a boolean <br>";
}

if (is_bool($b) === false) {
    echo "No, this is not a boolean";
}
echo "<br><br>";

// is_int()
echo "***** <b>is_int()</b> ****** </br></br>";
$values = array(23, "23", 23.5, "23.5", null, true, false);
foreach ( $values as $value) {
    echo var_export($value); echo " = "; echo var_dump(is_int($value));
    echo '<br>';
}
echo "<br><br>";

// is_object()
echo "***** <b>is_object()</b> ****** </br></br>";

function get_student($obj) {
    if (!is_object($obj)) {
        return false;
    }

    return $obj->students;
}

$obj=new stdClass();
$obj->students = array('Junaed','Sourav','Saimun');

var_dump(get_student(null));
echo "<br>";
var_dump(get_student($obj));

// isset()
echo "</br></br>***** <b>isset()</b> ****** </br></br>";
$var='';
if (isset($var)) { // is here the text will be printed.
    echo " this var is set so I will print<br>";
}

$a="test";
$b = "testno2";

var_dump(isset($a)); echo '<br>';
var_dump(isset($b)); echo '<br>';

unset($a);

var_dump(isset($a)); echo '<br>';
var_dump(isset($b)); echo '<br>';

$foo = null;
var_dump(isset($foo)); echo '<br><br>';

$a=array('one' => 1, 'two' => NULL, 'three' => array('d' => 'doll'));

var_dump(isset($a['one'])); echo '<br>';
var_dump(isset($a['four'])); echo '<br>';
var_dump(isset($a['two'])); echo '<br><br>';

var_dump(array_key_exists('two',$a)); echo '<br><br>'; // this function is for checking NULL key values;

// now checking deeper array values
var_dump(isset($a['three']['d'])); echo '<br>';
var_dump(isset($a['three']['b'])); echo '<br>';
var_dump(isset($a['cake']['d']['b'])); echo '<br><br>';

// print_r()
echo "</br></br>***** <b>print_r()</b> ****** </br></br>";
echo "<pre>";
$a = array('1' => 'One', '2' => 'Two', '3' => 'Three', '4' => array('I','We','Me'));
print_r($a); echo '<br></pre>';

$b = array('A' => 'Araf', 'H' => 'Hossain', 'Roman' => array('I','II','III'));
$results = print_r($b,true);
echo $results; echo '<br><br>';

// serialize() and unserialize();
echo "</br></br>***** <b>serialize() and unserialize()</b> ****** </br></br>";
echo "I didn't practice those function";